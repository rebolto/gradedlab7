public class Board{
private Square[][] tictactoeBoard;
	
public Board(){
	this.tictactoeBoard= new Square[3][3];
	for(int i =0;i<tictactoeBoard.length;i++){
		for(int j =0; j<tictactoeBoard[i].length;j++){
			tictactoeBoard[i][j]=Square.Blank;
		}
	}
}

public String toString(){
	String str="";
	for(int i =0;i<tictactoeBoard.length;i++){
		for(int j=0;j<tictactoeBoard[i].length;j++){
			str+=tictactoeBoard[i][j]+", ";
		}
		str+="\n";
	}
	return str;
		}
			
		
		
	
public boolean placeToken(int row,int col, Square playerToken){
	if(row<0||row>2 || col<0 || col>2){
		return false;
	}
		if(tictactoeBoard[row][col]==Square.Blank){
			tictactoeBoard[row][col]=playerToken;
			return true;
		}
		return false;
	}
public boolean checkIfFull(){
	for(int i =0;i<tictactoeBoard.length;i++){
		for(int j =0;j<tictactoeBoard[i].length;j++){
			if(tictactoeBoard[i][j]==Square.Blank){
			return false;
			}	
		}
	}
	return true;
}

private boolean checkIfWinningHorizontal(Square playerToken){
	for(int i=0;i<tictactoeBoard.length;i++){
		if(tictactoeBoard[i][0]==playerToken&&tictactoeBoard[i][1]==playerToken&&tictactoeBoard[i][2]==playerToken){
			return true;
		}
	}
	return false;
	
}
private boolean checkIfWinningVertical(Square playerToken){
	for(int i =0; i<tictactoeBoard.length;i++){
		if(tictactoeBoard[0][i]==playerToken&&tictactoeBoard[1][i]==playerToken&&tictactoeBoard[2][i]==playerToken){
			return true;
		}
	}
	return false;
}

//Bonus
private boolean checkIfWinningDiagonal(Square playerToken){
	if(tictactoeBoard[0][0]==playerToken&&tictactoeBoard[1][1]==playerToken&&tictactoeBoard[2][2]==playerToken || tictactoeBoard[2][0]==playerToken&&tictactoeBoard[1][1]==playerToken&&tictactoeBoard[0][2]==playerToken){
		return true;
	}
	return false;
}

public boolean checkIfWinning(Square playerToken){
	if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken)){
		return true;
	}
	return false;
}

















}















